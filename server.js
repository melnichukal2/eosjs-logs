var connect = require('connect');
var serveStatic = require('serve-static');
var ecc = require('eosjs-ecc');
var PrivateKey = require('./node_modules/eosjs-ecc/lib/key_private.js');
var PublicKey = require('./node_modules/eosjs-ecc/lib/key_public.js');
var Signature = require('./node_modules/eosjs-ecc/lib/signature.js');
var hash = require('./node_modules/eosjs-ecc/lib/hash.js');
//var Eos = require('./dist/eos.js');

console.log(ecc);
const seed = "a2b7073250343cce29eb7a73b3b50e91317b9c61504ec5e07c5e42890cb389ae03e6d5d2de9fc2c0c01b430a77464ce2d96a2ff281e2e19c19123955bd794390";
console.log("__HASH:" + new Uint8Array(hash.sha256(seed)));
const privateKey = PrivateKey.fromSeed(seed);
const privateKeyHex = privateKey.toBuffer().toString('hex');
console.log("PRIVATE KEY HEX:" + privateKeyHex);
console.log("PRIVATE KEY:" + privateKey.toString());
const d = privateKey.d
console.log("D:" + d);
const publicKey = privateKey.toPublic();
const publicKeyHex = publicKey.toBuffer().toString('hex');
console.log("PUBLIC KEY HEX:" + publicKeyHex);
console.log("PUBLIC KEY: " + publicKey);
const address = publicKey.toString();
console.log("ADDRESS:" + address);

// http://jungle.cryptolions.io:
// chainid 038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca
const eosConf = {
  chainId: null, // cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f used as a default internally // 32 byte (64 char) hex string
  keyProvider: ['PrivateKeys...'], // WIF string or array of keys..
  httpEndpoint: null,
  expireInSeconds: 60,
  broadcast: false,
  verbose: false, // API activity
  sign: true
};

//const eos = Eos(eosConf);

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function makeid(n) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < n; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


for (let i = 0; i < 0; ++i) {
	const randInt = getRandomInt(6, 168);
	const rand = makeid(randInt);//Math.random().toString(36).substring(randInt);
	console.log("it:" + i + "randInt:" + randInt + " rand: " + rand);

	const signature = Signature.sign(Buffer.from(rand, 'utf8'), privateKey);
	console.log("SIGNATURE BUFF HEX: " + signature.toBuffer().toString('hex'));
}

//const signature = Signature.sign(Buffer.from('rqxoj1L2oBNTBGFVsF63h5fbf7DQNydUa', 'utf8'), privateKey);//Signature.sign('Hello, world!', privateKey);//ecc.sign('hello, world', privateKey, 'utf8');
//console.log("SIGNATURE HEX: " + signature.toBuffer().toString('hex'));
//console.log("SIGNATURE: " + signature.toString());


const signature2 = Signature.sign(Buffer.from('CXVxhAlGft9JD3LdGHqoTwKgcYUpcdbotLHYh4qfzhj5Qiyr', 'utf8'), privateKey);
console.log("SIGNATURE HEX2: " + signature2.toBuffer().toString('hex'));
console.log("SIGNATURE2: " + signature2.toString());

//const signature3 = Signature.sign(Buffer.from('Hello, world!', 'utf8'), privateKey);
// console.log("SIGNATURE HEX3: " + signature3.toBuffer().toString('hex'));
// console.log("SIGNATURE3: " + signature3.toString());
connect().use(serveStatic(__dirname)).listen(8080, function(){
	
    console.log('Server running on 8080...');
});


